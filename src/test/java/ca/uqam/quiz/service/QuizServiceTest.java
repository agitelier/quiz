package ca.uqam.quiz.service;

import ca.uqam.quiz.QuizApp;
import ca.uqam.quiz.config.TestSecurityConfiguration;
import ca.uqam.quiz.domain.Quiz;
import ca.uqam.quiz.domain.enumeration.Language;
import ca.uqam.quiz.domain.enumeration.Mode;
import ca.uqam.quiz.repository.QuizRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration tests for {@link QuizService}.
 */
@SpringBootTest(classes = {QuizApp.class, TestSecurityConfiguration.class})
public class QuizServiceTest {

    private static final String DEFAULT_ID = "603ffdeab860985e039f7619";

    private static final String DEFAULT_TITLE = "Test quiz title";

    private static final String DEFAULT_DESCRIPTION = "Test quiz description";

    private static final Language DEFAULT_LANGUAGE = Language.FRENCH;

    private static final Mode DEFAULT_MODE = Mode.SOLO;

    private Quiz thisQuiz = new Quiz();

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private QuizService quizService;

    @BeforeEach
    public void init() {
        quizRepository.deleteAll();

        thisQuiz.setId(DEFAULT_ID);
        thisQuiz.setTitle(DEFAULT_TITLE);
        thisQuiz.setLanguage(DEFAULT_LANGUAGE);
        thisQuiz.setMode(DEFAULT_MODE);
        thisQuiz.setDescription(DEFAULT_DESCRIPTION);
    }

    @Test
    public void assertThatFormatIsCorrect() {
        Optional<Quiz> quiz = Optional.of(thisQuiz);

        HashMap<String, String> testQuiz = new HashMap<>(
            Map.of("id", quiz.get().getId(),
                "title", quiz.get().getTitle(),
                "description", quiz.get().getDescription()));

        assertThat(quizService.format(quiz)).isEqualTo(testQuiz);
    }

    @Test
    public void assertThatFormatListIsCorrect() {
        ArrayList<HashMap<String, String>> returnQuizList = new ArrayList<>();
        ArrayList<Quiz> quizList = new ArrayList<>();
        quizList.add(thisQuiz);
        quizList.add(thisQuiz);

        for (Quiz q : quizList){
            HashMap<String, String> tempQuiz = new HashMap<>();
            tempQuiz.put("id", q.getId());
            tempQuiz.put("title", q.getTitle());
            tempQuiz.put("description", q.getDescription());
            returnQuizList.add(tempQuiz);
        }

        assertThat(quizService.formatList(quizList)).isEqualTo(returnQuizList);
    }
}
