package ca.uqam.quiz;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("ca.uqam.quiz");

        noClasses()
            .that()
                .resideInAnyPackage("ca.uqam.quiz.service..")
            .or()
                .resideInAnyPackage("ca.uqam.quiz.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..ca.uqam.quiz.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
