package ca.uqam.quiz.web.rest;

import ca.uqam.quiz.QuizApp;
import ca.uqam.quiz.config.TestSecurityConfiguration;
import ca.uqam.quiz.domain.Results;
import ca.uqam.quiz.repository.ResultsRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ResultsResource} REST controller.
 */
@SpringBootTest(classes = { QuizApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class ResultsResourceIT {

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_ID_QUIZ = "AAAAAAAAAA";
    private static final String UPDATED_ID_QUIZ = "BBBBBBBBBB";

    private static final String DEFAULT_ID_QUESTION = "AAAAAAAAAA";
    private static final String UPDATED_ID_QUESTION = "BBBBBBBBBB";

    private static final String DEFAULT_ANSWER = "AAAAAAAAAA";
    private static final String UPDATED_ANSWER = "BBBBBBBBBB";

    private static final String DEFAULT_ID_SUB_GROUP = "AAAAAAAAAA";
    private static final String UPDATED_ID_SUB_GROUP = "BBBBBBBBBB";

    @Autowired
    private ResultsRepository resultsRepository;

    @Autowired
    private MockMvc restResultsMockMvc;

    private Results results;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Results createEntity() {
        Results results = new Results()
            .email(DEFAULT_EMAIL)
            .idQuiz(DEFAULT_ID_QUIZ)
            .idQuestion(DEFAULT_ID_QUESTION)
            .answer(DEFAULT_ANSWER)
            .idSubGroup(DEFAULT_ID_SUB_GROUP);
        return results;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Results createUpdatedEntity() {
        Results results = new Results()
            .email(UPDATED_EMAIL)
            .idQuiz(UPDATED_ID_QUIZ)
            .idQuestion(UPDATED_ID_QUESTION)
            .answer(UPDATED_ANSWER)
            .idSubGroup(UPDATED_ID_SUB_GROUP);
        return results;
    }

    @BeforeEach
    public void initTest() {
        resultsRepository.deleteAll();
        results = createEntity();
    }

    @Test
    public void createResults() throws Exception {
        int databaseSizeBeforeCreate = resultsRepository.findAll().size();
        // Create the Results
        restResultsMockMvc.perform(post("/api/results").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(results)))
            .andExpect(status().isCreated());

        // Validate the Results in the database
        List<Results> resultsList = resultsRepository.findAll();
        assertThat(resultsList).hasSize(databaseSizeBeforeCreate + 1);
        Results testResults = resultsList.get(resultsList.size() - 1);
        assertThat(testResults.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testResults.getIdQuiz()).isEqualTo(DEFAULT_ID_QUIZ);
        assertThat(testResults.getIdQuestion()).isEqualTo(DEFAULT_ID_QUESTION);
        assertThat(testResults.getAnswer()).isEqualTo(DEFAULT_ANSWER);
        assertThat(testResults.getIdSubGroup()).isEqualTo(DEFAULT_ID_SUB_GROUP);
    }

    @Test
    public void createResultsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = resultsRepository.findAll().size();

        // Create the Results with an existing ID
        results.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restResultsMockMvc.perform(post("/api/results").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(results)))
            .andExpect(status().isBadRequest());

        // Validate the Results in the database
        List<Results> resultsList = resultsRepository.findAll();
        assertThat(resultsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = resultsRepository.findAll().size();
        // set the field null
        results.setEmail(null);

        // Create the Results, which fails.


        restResultsMockMvc.perform(post("/api/results").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(results)))
            .andExpect(status().isBadRequest());

        List<Results> resultsList = resultsRepository.findAll();
        assertThat(resultsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkIdQuizIsRequired() throws Exception {
        int databaseSizeBeforeTest = resultsRepository.findAll().size();
        // set the field null
        results.setIdQuiz(null);

        // Create the Results, which fails.


        restResultsMockMvc.perform(post("/api/results").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(results)))
            .andExpect(status().isBadRequest());

        List<Results> resultsList = resultsRepository.findAll();
        assertThat(resultsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkIdQuestionIsRequired() throws Exception {
        int databaseSizeBeforeTest = resultsRepository.findAll().size();
        // set the field null
        results.setIdQuestion(null);

        // Create the Results, which fails.


        restResultsMockMvc.perform(post("/api/results").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(results)))
            .andExpect(status().isBadRequest());

        List<Results> resultsList = resultsRepository.findAll();
        assertThat(resultsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkAnswerIsRequired() throws Exception {
        int databaseSizeBeforeTest = resultsRepository.findAll().size();
        // set the field null
        results.setAnswer(null);

        // Create the Results, which fails.


        restResultsMockMvc.perform(post("/api/results").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(results)))
            .andExpect(status().isBadRequest());

        List<Results> resultsList = resultsRepository.findAll();
        assertThat(resultsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllResults() throws Exception {
        // Initialize the database
        resultsRepository.save(results);

        // Get all the resultsList
        restResultsMockMvc.perform(get("/api/results?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(results.getId())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].idQuiz").value(hasItem(DEFAULT_ID_QUIZ)))
            .andExpect(jsonPath("$.[*].idQuestion").value(hasItem(DEFAULT_ID_QUESTION)))
            .andExpect(jsonPath("$.[*].answer").value(hasItem(DEFAULT_ANSWER)))
            .andExpect(jsonPath("$.[*].idSubGroup").value(hasItem(DEFAULT_ID_SUB_GROUP)));
    }
    
    @Test
    public void getResults() throws Exception {
        // Initialize the database
        resultsRepository.save(results);

        // Get the results
        restResultsMockMvc.perform(get("/api/results/{id}", results.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(results.getId()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.idQuiz").value(DEFAULT_ID_QUIZ))
            .andExpect(jsonPath("$.idQuestion").value(DEFAULT_ID_QUESTION))
            .andExpect(jsonPath("$.answer").value(DEFAULT_ANSWER))
            .andExpect(jsonPath("$.idSubGroup").value(DEFAULT_ID_SUB_GROUP));
    }
    @Test
    public void getNonExistingResults() throws Exception {
        // Get the results
        restResultsMockMvc.perform(get("/api/results/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateResults() throws Exception {
        // Initialize the database
        resultsRepository.save(results);

        int databaseSizeBeforeUpdate = resultsRepository.findAll().size();

        // Update the results
        Results updatedResults = resultsRepository.findById(results.getId()).get();
        updatedResults
            .email(UPDATED_EMAIL)
            .idQuiz(UPDATED_ID_QUIZ)
            .idQuestion(UPDATED_ID_QUESTION)
            .answer(UPDATED_ANSWER)
            .idSubGroup(UPDATED_ID_SUB_GROUP);

        restResultsMockMvc.perform(put("/api/results").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedResults)))
            .andExpect(status().isOk());

        // Validate the Results in the database
        List<Results> resultsList = resultsRepository.findAll();
        assertThat(resultsList).hasSize(databaseSizeBeforeUpdate);
        Results testResults = resultsList.get(resultsList.size() - 1);
        assertThat(testResults.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testResults.getIdQuiz()).isEqualTo(UPDATED_ID_QUIZ);
        assertThat(testResults.getIdQuestion()).isEqualTo(UPDATED_ID_QUESTION);
        assertThat(testResults.getAnswer()).isEqualTo(UPDATED_ANSWER);
        assertThat(testResults.getIdSubGroup()).isEqualTo(UPDATED_ID_SUB_GROUP);
    }

    @Test
    public void updateNonExistingResults() throws Exception {
        int databaseSizeBeforeUpdate = resultsRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restResultsMockMvc.perform(put("/api/results").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(results)))
            .andExpect(status().isBadRequest());

        // Validate the Results in the database
        List<Results> resultsList = resultsRepository.findAll();
        assertThat(resultsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteResults() throws Exception {
        // Initialize the database
        resultsRepository.save(results);

        int databaseSizeBeforeDelete = resultsRepository.findAll().size();

        // Delete the results
        restResultsMockMvc.perform(delete("/api/results/{id}", results.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Results> resultsList = resultsRepository.findAll();
        assertThat(resultsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
