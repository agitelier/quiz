package ca.uqam.quiz.web.rest;

import ca.uqam.quiz.QuizApp;
import ca.uqam.quiz.config.TestSecurityConfiguration;
import ca.uqam.quiz.domain.Choice;
import ca.uqam.quiz.repository.ChoiceRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ca.uqam.quiz.domain.enumeration.Language;
/**
 * Integration tests for the {@link ChoiceResource} REST controller.
 */
@SpringBootTest(classes = { QuizApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class ChoiceResourceIT {

    private static final Language DEFAULT_LANGUAGE = Language.FRENCH;
    private static final Language UPDATED_LANGUAGE = Language.ENGLISH;

    private static final String DEFAULT_DISPLAY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DISPLAY_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_VALUE = 1;
    private static final Integer UPDATED_VALUE = 2;

    private static final Boolean DEFAULT_IS_GOOD = false;
    private static final Boolean UPDATED_IS_GOOD = true;

    @Autowired
    private ChoiceRepository choiceRepository;

    @Autowired
    private MockMvc restChoiceMockMvc;

    private Choice choice;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Choice createEntity() {
        Choice choice = new Choice()
            .language(DEFAULT_LANGUAGE)
            .displayName(DEFAULT_DISPLAY_NAME)
            .value(DEFAULT_VALUE)
            .isGood(DEFAULT_IS_GOOD);
        return choice;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Choice createUpdatedEntity() {
        Choice choice = new Choice()
            .language(UPDATED_LANGUAGE)
            .displayName(UPDATED_DISPLAY_NAME)
            .value(UPDATED_VALUE)
            .isGood(UPDATED_IS_GOOD);
        return choice;
    }

    @BeforeEach
    public void initTest() {
        choiceRepository.deleteAll();
        choice = createEntity();
    }

    @Test
    public void createChoice() throws Exception {
        int databaseSizeBeforeCreate = choiceRepository.findAll().size();
        // Create the Choice
        restChoiceMockMvc.perform(post("/api/choices").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(choice)))
            .andExpect(status().isCreated());

        // Validate the Choice in the database
        List<Choice> choiceList = choiceRepository.findAll();
        assertThat(choiceList).hasSize(databaseSizeBeforeCreate + 1);
        Choice testChoice = choiceList.get(choiceList.size() - 1);
        assertThat(testChoice.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
        assertThat(testChoice.getDisplayName()).isEqualTo(DEFAULT_DISPLAY_NAME);
        assertThat(testChoice.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testChoice.isIsGood()).isEqualTo(DEFAULT_IS_GOOD);
    }

    @Test
    public void createChoiceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = choiceRepository.findAll().size();

        // Create the Choice with an existing ID
        choice.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restChoiceMockMvc.perform(post("/api/choices").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(choice)))
            .andExpect(status().isBadRequest());

        // Validate the Choice in the database
        List<Choice> choiceList = choiceRepository.findAll();
        assertThat(choiceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void checkLanguageIsRequired() throws Exception {
        int databaseSizeBeforeTest = choiceRepository.findAll().size();
        // set the field null
        choice.setLanguage(null);

        // Create the Choice, which fails.


        restChoiceMockMvc.perform(post("/api/choices").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(choice)))
            .andExpect(status().isBadRequest());

        List<Choice> choiceList = choiceRepository.findAll();
        assertThat(choiceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkDisplayNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = choiceRepository.findAll().size();
        // set the field null
        choice.setDisplayName(null);

        // Create the Choice, which fails.


        restChoiceMockMvc.perform(post("/api/choices").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(choice)))
            .andExpect(status().isBadRequest());

        List<Choice> choiceList = choiceRepository.findAll();
        assertThat(choiceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllChoices() throws Exception {
        // Initialize the database
        choiceRepository.save(choice);

        // Get all the choiceList
        restChoiceMockMvc.perform(get("/api/choices?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(choice.getId())))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE.toString())))
            .andExpect(jsonPath("$.[*].displayName").value(hasItem(DEFAULT_DISPLAY_NAME)))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE)))
            .andExpect(jsonPath("$.[*].isGood").value(hasItem(DEFAULT_IS_GOOD.booleanValue())));
    }
    
    @Test
    public void getChoice() throws Exception {
        // Initialize the database
        choiceRepository.save(choice);

        // Get the choice
        restChoiceMockMvc.perform(get("/api/choices/{id}", choice.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(choice.getId()))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE.toString()))
            .andExpect(jsonPath("$.displayName").value(DEFAULT_DISPLAY_NAME))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE))
            .andExpect(jsonPath("$.isGood").value(DEFAULT_IS_GOOD.booleanValue()));
    }
    @Test
    public void getNonExistingChoice() throws Exception {
        // Get the choice
        restChoiceMockMvc.perform(get("/api/choices/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateChoice() throws Exception {
        // Initialize the database
        choiceRepository.save(choice);

        int databaseSizeBeforeUpdate = choiceRepository.findAll().size();

        // Update the choice
        Choice updatedChoice = choiceRepository.findById(choice.getId()).get();
        updatedChoice
            .language(UPDATED_LANGUAGE)
            .displayName(UPDATED_DISPLAY_NAME)
            .value(UPDATED_VALUE)
            .isGood(UPDATED_IS_GOOD);

        restChoiceMockMvc.perform(put("/api/choices").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedChoice)))
            .andExpect(status().isOk());

        // Validate the Choice in the database
        List<Choice> choiceList = choiceRepository.findAll();
        assertThat(choiceList).hasSize(databaseSizeBeforeUpdate);
        Choice testChoice = choiceList.get(choiceList.size() - 1);
        assertThat(testChoice.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testChoice.getDisplayName()).isEqualTo(UPDATED_DISPLAY_NAME);
        assertThat(testChoice.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testChoice.isIsGood()).isEqualTo(UPDATED_IS_GOOD);
    }

    @Test
    public void updateNonExistingChoice() throws Exception {
        int databaseSizeBeforeUpdate = choiceRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restChoiceMockMvc.perform(put("/api/choices").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(choice)))
            .andExpect(status().isBadRequest());

        // Validate the Choice in the database
        List<Choice> choiceList = choiceRepository.findAll();
        assertThat(choiceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteChoice() throws Exception {
        // Initialize the database
        choiceRepository.save(choice);

        int databaseSizeBeforeDelete = choiceRepository.findAll().size();

        // Delete the choice
        restChoiceMockMvc.perform(delete("/api/choices/{id}", choice.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Choice> choiceList = choiceRepository.findAll();
        assertThat(choiceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
