/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import ResultsDetailComponent from '@/entities/results/results-details.vue';
import ResultsClass from '@/entities/results/results-details.component';
import ResultsService from '@/entities/results/results.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Results Management Detail Component', () => {
    let wrapper: Wrapper<ResultsClass>;
    let comp: ResultsClass;
    let resultsServiceStub: SinonStubbedInstance<ResultsService>;

    beforeEach(() => {
      resultsServiceStub = sinon.createStubInstance<ResultsService>(ResultsService);

      wrapper = shallowMount<ResultsClass>(ResultsDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { resultsService: () => resultsServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundResults = { id: '123' };
        resultsServiceStub.find.resolves(foundResults);

        // WHEN
        comp.retrieveResults('123');
        await comp.$nextTick();

        // THEN
        expect(comp.results).toBe(foundResults);
      });
    });
  });
});
