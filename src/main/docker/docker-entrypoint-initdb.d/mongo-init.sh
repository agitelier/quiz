#!/bin/bash

mongoimport --db quiz --collection choice --file /datamongo/choice.json --jsonArray
mongoimport --db quiz --collection question --file /datamongo/question.json --jsonArray
mongoimport --db quiz --collection quiz --file /datamongo/quiz.json --jsonArray
#mongoimport --db quiz --collection results --file /datamongo/results.json --jsonArray

echo "data imported to mongo db"
