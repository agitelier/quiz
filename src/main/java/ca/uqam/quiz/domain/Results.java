package ca.uqam.quiz.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Results.
 */
@Document(collection = "results")
public class Results implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("email")
    private String email;

    @NotNull
    @Field("id_quiz")
    private String idQuiz;

    @NotNull
    @Field("id_question")
    private String idQuestion;

    @NotNull
    @Field("answer")
    private String answer;

    @Field("id_sub_group")
    private String idSubGroup;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public Results email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdQuiz() {
        return idQuiz;
    }

    public Results idQuiz(String idQuiz) {
        this.idQuiz = idQuiz;
        return this;
    }

    public void setIdQuiz(String idQuiz) {
        this.idQuiz = idQuiz;
    }

    public String getIdQuestion() {
        return idQuestion;
    }

    public Results idQuestion(String idQuestion) {
        this.idQuestion = idQuestion;
        return this;
    }

    public void setIdQuestion(String idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getAnswer() {
        return answer;
    }

    public Results answer(String answer) {
        this.answer = answer;
        return this;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getIdSubGroup() {
        return idSubGroup;
    }

    public Results idSubGroup(String idSubGroup) {
        this.idSubGroup = idSubGroup;
        return this;
    }

    public void setIdSubGroup(String idSubGroup) {
        this.idSubGroup = idSubGroup;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Results)) {
            return false;
        }
        return id != null && id.equals(((Results) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Results{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            ", idQuiz='" + getIdQuiz() + "'" +
            ", idQuestion='" + getIdQuestion() + "'" +
            ", answer='" + getAnswer() + "'" +
            ", idSubGroup='" + getIdSubGroup() + "'" +
            "}";
    }
}
