package ca.uqam.quiz.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import ca.uqam.quiz.domain.enumeration.Language;

/**
 * A Choice.
 */
@Document(collection = "choice")
public class Choice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("language")
    private Language language;

    @NotNull
    @Field("display_name")
    private String displayName;

    @Field("value")
    private Integer value;

    @Field("is_good")
    private Boolean isGood;

    @DBRef
    @Field("questions")
    @JsonIgnore
    private Set<Question> questions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Language getLanguage() {
        return language;
    }

    public Choice language(Language language) {
        this.language = language;
        return this;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Choice displayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getValue() {
        return value;
    }

    public Choice value(Integer value) {
        this.value = value;
        return this;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Boolean isIsGood() {
        return isGood;
    }

    public Choice isGood(Boolean isGood) {
        this.isGood = isGood;
        return this;
    }

    public void setIsGood(Boolean isGood) {
        this.isGood = isGood;
    }

    public Set<Question> getQuestions() {
        return questions;
    }

    public Choice questions(Set<Question> questions) {
        this.questions = questions;
        return this;
    }

    public Choice addQuestion(Question question) {
        this.questions.add(question);
        question.getChoices().add(this);
        return this;
    }

    public Choice removeQuestion(Question question) {
        this.questions.remove(question);
        question.getChoices().remove(this);
        return this;
    }

    public void setQuestions(Set<Question> questions) {
        this.questions = questions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Choice)) {
            return false;
        }
        return id != null && id.equals(((Choice) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Choice{" +
            "id=" + getId() +
            ", language='" + getLanguage() + "'" +
            ", displayName='" + getDisplayName() + "'" +
            ", value=" + getValue() +
            ", isGood='" + isIsGood() + "'" +
            "}";
    }
}
