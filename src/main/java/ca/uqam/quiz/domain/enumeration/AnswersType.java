package ca.uqam.quiz.domain.enumeration;

/**
 * The AnswersType enumeration.
 */
public enum AnswersType {
    OPEN, ONE, MULTI
}
