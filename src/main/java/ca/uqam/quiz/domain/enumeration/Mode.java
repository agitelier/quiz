package ca.uqam.quiz.domain.enumeration;

/**
 * The Mode enumeration.
 */
public enum Mode {
    SOLO, GROUP
}
