package ca.uqam.quiz.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    FRENCH, ENGLISH
}
