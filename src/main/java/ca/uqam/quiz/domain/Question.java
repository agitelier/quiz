package ca.uqam.quiz.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import ca.uqam.quiz.domain.enumeration.Language;

import ca.uqam.quiz.domain.enumeration.AnswersType;

/**
 * A Question.
 */
@Document(collection = "question")
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("language")
    private Language language;

    @NotNull
    @Field("name")
    private String name;

    @NotNull
    @Field("answers_type")
    private AnswersType answersType;

    @DBRef
    @Field("choices")
    private Set<Choice> choices = new HashSet<>();

    @DBRef
    @Field("quizzes")
    @JsonIgnore
    private Set<Quiz> quizzes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Language getLanguage() {
        return language;
    }

    public Question language(Language language) {
        this.language = language;
        return this;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public Question name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AnswersType getAnswersType() {
        return answersType;
    }

    public Question answersType(AnswersType answersType) {
        this.answersType = answersType;
        return this;
    }

    public void setAnswersType(AnswersType answersType) {
        this.answersType = answersType;
    }

    public Set<Choice> getChoices() {
        return choices;
    }

    public Question choices(Set<Choice> choices) {
        this.choices = choices;
        return this;
    }

    public Question addChoice(Choice choice) {
        this.choices.add(choice);
        choice.getQuestions().add(this);
        return this;
    }

    public Question removeChoice(Choice choice) {
        this.choices.remove(choice);
        choice.getQuestions().remove(this);
        return this;
    }

    public void setChoices(Set<Choice> choices) {
        this.choices = choices;
    }

    public Set<Quiz> getQuizzes() {
        return quizzes;
    }

    public Question quizzes(Set<Quiz> quizzes) {
        this.quizzes = quizzes;
        return this;
    }

    public Question addQuiz(Quiz quiz) {
        this.quizzes.add(quiz);
        quiz.getQuestions().add(this);
        return this;
    }

    public Question removeQuiz(Quiz quiz) {
        this.quizzes.remove(quiz);
        quiz.getQuestions().remove(this);
        return this;
    }

    public void setQuizzes(Set<Quiz> quizzes) {
        this.quizzes = quizzes;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Question)) {
            return false;
        }
        return id != null && id.equals(((Question) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Question{" +
            "id=" + getId() +
            ", language='" + getLanguage() + "'" +
            ", name='" + getName() + "'" +
            ", answersType='" + getAnswersType() + "'" +
            "}";
    }
}
