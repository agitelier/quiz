package ca.uqam.quiz.service;

import ca.uqam.quiz.domain.Quiz;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class QuizService {

    public static HashMap<String, String> format(Optional<Quiz> quiz){
        HashMap<String, String> response = new HashMap<>();
        if(quiz.isPresent()){
            response.put("id", quiz.get().getId());
            response.put("title", quiz.get().getTitle());
            response.put("description", quiz.get().getDescription());
        }else{
            response.put("error", "Id not found");
        }
        return response;
    }

    public static ArrayList<HashMap<String, String>> formatList(List<Quiz> quizList){
        ArrayList<HashMap<String, String>> returnQuizList = new ArrayList<>();
        for (Quiz q : quizList){
            HashMap<String, String> tempQuiz = new HashMap<>();
            tempQuiz.put("id", q.getId());
            tempQuiz.put("title", q.getTitle());
            tempQuiz.put("description", q.getDescription());
            returnQuizList.add(tempQuiz);
        }
        return returnQuizList;
    }
}
