package ca.uqam.quiz.service;

import ca.uqam.quiz.domain.Results;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
public class ResultsService {

    public static HashMap<String, String> format(Optional<Results> results){
        HashMap<String, String> response = new HashMap<>();
        if(results.isPresent()){
            response.put("id", results.get().getId());
            response.put("idQuiz", results.get().getIdQuiz());
            response.put("idQuestion", results.get().getIdQuestion());
            response.put("idSubGroup", results.get().getIdSubGroup());
            response.put("email", results.get().getEmail());
            response.put("answer", results.get().getAnswer());
        }else{
            response.put("error", "Id not found");
        }
        return response;
    }

    public static ArrayList<HashMap<String, String>> formatList(List<Results> resultsList){
        ArrayList<HashMap<String, String>> returnResultsList = new ArrayList<>();
        for (Results r : resultsList){
            HashMap<String, String> tempResults = new HashMap<>();
            tempResults.put("id", r.getId());
            tempResults.put("idQuiz", r.getIdQuiz());
            tempResults.put("idQuestion", r.getIdQuestion());
            tempResults.put("idSubGroup", r.getIdSubGroup());
            tempResults.put("email", r.getEmail());
            tempResults.put("answer", r.getAnswer());
            returnResultsList.add(tempResults);
        }
        return returnResultsList;
    }
}
