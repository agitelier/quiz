package ca.uqam.quiz.repository;

import ca.uqam.quiz.domain.Quiz;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Quiz entity.
 */
@Repository
public interface QuizRepository extends MongoRepository<Quiz, String> {

    @Query("{}")
    Page<Quiz> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<Quiz> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<Quiz> findOneWithEagerRelationships(String id);
}
