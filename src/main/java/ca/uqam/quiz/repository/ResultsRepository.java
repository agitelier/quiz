package ca.uqam.quiz.repository;

import ca.uqam.quiz.domain.Results;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Results entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResultsRepository extends MongoRepository<Results, String> {
    Page<Results> findAllByIdQuiz(Pageable pageable, String id);

    Page<Results> findAllByIdQuizAndEmail(Pageable pageable, String idQuiz, String email);

    Page<Results> findAllByIdQuizAndIdSubGroup(Pageable pageable, String idQuiz, String idSubGroup);
}
