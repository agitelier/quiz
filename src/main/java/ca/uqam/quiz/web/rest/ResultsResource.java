package ca.uqam.quiz.web.rest;

import ca.uqam.quiz.domain.Results;
import ca.uqam.quiz.repository.ResultsRepository;
import ca.uqam.quiz.service.ResultsService;
import ca.uqam.quiz.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ca.uqam.quiz.domain.Results}.
 */
@RestController
@RequestMapping("/api")
public class ResultsResource {

    private final Logger log = LoggerFactory.getLogger(ResultsResource.class);

    private static final String ENTITY_NAME = "results";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ResultsRepository resultsRepository;

    public ResultsResource(ResultsRepository resultsRepository) {
        this.resultsRepository = resultsRepository;
    }

    /**
     * {@code POST  /results} : Create a new results.
     *
     * @param results the results to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new results, or with status {@code 400 (Bad Request)} if the results has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/results")
    public ResponseEntity<Results> createResults(@Valid @RequestBody Results results) throws URISyntaxException {
        log.debug("REST request to save Results : {}", results);
        if (results.getId() != null) {
            throw new BadRequestAlertException("A new results cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Results result = resultsRepository.save(results);
        return ResponseEntity.created(new URI("/api/results/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /results} : Updates an existing results.
     *
     * @param results the results to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated results,
     * or with status {@code 400 (Bad Request)} if the results is not valid,
     * or with status {@code 500 (Internal Server Error)} if the results couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/results")
    public ResponseEntity<Results> updateResults(@Valid @RequestBody Results results) throws URISyntaxException {
        log.debug("REST request to update Results : {}", results);
        if (results.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Results result = resultsRepository.save(results);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, results.getId()))
            .body(result);
    }

    /**
     * {@code GET  /results} : get all the results.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of results in body.
     */
    @GetMapping("/results")
    public ResponseEntity<List<Results>> getAllResults(Pageable pageable) {
        log.debug("REST request to get a page of Results");
        Page<Results> page = resultsRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /results/:id} : get the "id" results.
     *
     * @param id the id of the results to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the results, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/results/{id}")
    public ResponseEntity<Results> getResults(@PathVariable String id) {
        log.debug("REST request to get Results : {}", id);
        Optional<Results> results = resultsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(results);
    }

    /**
     * {@code DELETE  /results/:id} : delete the "id" results.
     *
     * @param id the id of the results to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/results/{id}")
    public ResponseEntity<Void> deleteResults(@PathVariable String id) {
        log.debug("REST request to delete Results : {}", id);
        resultsRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }

    /**
     * {@code GET  /results/findAllForQuizId/{id}} : get all the results for specific quiz id.
     *
     * @param id the id of the related quiz id.
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of results in body.
     */
    @GetMapping("/results/findAllForQuizId/{id}")
    public ResponseEntity<List<Results>> getAllResultsForQuiz(Pageable pageable, @PathVariable String id) {
        log.debug("REST request to get a page of Results for specific quiz id");
        Page<Results> page = resultsRepository.findAllByIdQuiz(pageable, id);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /results/list} : get all the results.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the List of results, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/results/list")
    public ResponseEntity<ArrayList<HashMap<String, String>>> getAllResults() {
        log.debug("REST request to get all Results");
        List<Results> listResults = resultsRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(ResultsService.formatList(listResults));
    }

    /**
     * {@code GET  /results/findAllForQuizIdAndSubGroupId/{id}/{idSubGroup}} : get all the results for specific quiz id and sub-group
     *
     * @param id the id of the related quiz id.
     * @param idSubGroup the id of the related sub-group id.
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of results in body.
     */
    @GetMapping("/results/findAllForQuizIdAndSubGroupId/{id}/{idSubGroup}")
    public ResponseEntity<List<Results>> getAllResultsForQuizAndSubGroup(Pageable pageable, @PathVariable String id,
                                                              @RequestParam(required = false) String idSubGroup) {
        log.debug("REST request to get a page of Results for specific quiz id and id SubGroup");
        Page<Results> page;
        if (idSubGroup != null) {
            page =resultsRepository.findAllByIdQuizAndIdSubGroup(pageable, id, idSubGroup);
        } else {
            page =resultsRepository.findAllByIdQuiz(pageable, id);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
