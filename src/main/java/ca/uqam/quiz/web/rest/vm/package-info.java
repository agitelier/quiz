/**
 * View Models used by Spring MVC REST controllers.
 */
package ca.uqam.quiz.web.rest.vm;
