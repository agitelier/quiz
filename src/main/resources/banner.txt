
${AnsiColor.BLUE}    ________  ___  ___  ___  ________  ________
${AnsiColor.BLUE}   |\   __  \|\  \|\  \|\  \|\_____  \|\_____  \
${AnsiColor.BLUE}   \ \  \|\  \ \  \\\  \ \  \\|___/  /|\|___/  /|
${AnsiColor.BLUE}    \ \  \\\  \ \  \\\  \ \  \   /  / /    /  / /
${AnsiColor.GREEN}    \ \  \\\  \ \  \\\  \ \  \ /  /_/__  /  /_/__
${AnsiColor.GREEN}     \ \_____  \ \_______\ \__\\________\\________\
${AnsiColor.GREEN}      \|___| \__\|_______|\|__|\|_______|\|_______|
${AnsiColor.GREEN}            \|__|


${AnsiColor.BRIGHT_BLUE}:: JHipster 🤓  :: Running Spring Boot ${spring-boot.version} ::
:: https://www.jhipster.tech ::${AnsiColor.DEFAULT}
