import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore

// prettier-ignore
const Quiz = () => import('@/entities/quiz/quiz.vue');
// prettier-ignore
const QuizUpdate = () => import('@/entities/quiz/quiz-update.vue');
// prettier-ignore
const QuizDetails = () => import('@/entities/quiz/quiz-details.vue');
// prettier-ignore
const QuizCompare = () => import('@/entities/quiz/quiz-compare.vue');
// prettier-ignore
const QuizResults = () => import('@/entities/quiz/quiz-results.vue');
// prettier-ignore
const Question = () => import('@/entities/question/question.vue');
// prettier-ignore
const QuestionUpdate = () => import('@/entities/question/question-update.vue');
// prettier-ignore
const QuestionDetails = () => import('@/entities/question/question-details.vue');
// prettier-ignore
const Choice = () => import('@/entities/choice/choice.vue');
// prettier-ignore
const ChoiceUpdate = () => import('@/entities/choice/choice-update.vue');
// prettier-ignore
const ChoiceDetails = () => import('@/entities/choice/choice-details.vue');
// prettier-ignore
const Results = () => import('@/entities/results/results.vue');
// prettier-ignore
const ResultsUpdate = () => import('@/entities/results/results-update.vue');
// prettier-ignore
const ResultsDetails = () => import('@/entities/results/results-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default [
  {
    path: '/quiz',
    name: 'Quiz',
    component: Quiz,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/quiz/new',
    name: 'QuizCreate',
    component: QuizUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/quiz/:quizId/edit',
    name: 'QuizEdit',
    component: QuizUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/quiz/:quizId/view',
    name: 'QuizView',
    component: QuizDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/quiz/:quizId',
    name: 'QuizView',
    component: QuizDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/quiz/:quizId/:subgroupId',
    name: 'QuizView',
    component: QuizDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/quiz/:quizId/compare',
    name: 'QuizCompareResults',
    component: QuizCompare,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/quiz/:quizId/compare/:subgroupId',
    name: 'QuizCompareResults',
    component: QuizCompare,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/quiz/:quizId/viewResults',
    name: 'QuizViewResults',
    component: QuizResults,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/quiz/:quizId/viewResults/:subgroupId',
    name: 'QuizViewResults',
    component: QuizResults,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/question',
    name: 'Question',
    component: Question,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/question/new',
    name: 'QuestionCreate',
    component: QuestionUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/question/:questionId/edit',
    name: 'QuestionEdit',
    component: QuestionUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/question/:questionId/view',
    name: 'QuestionView',
    component: QuestionDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/choice',
    name: 'Choice',
    component: Choice,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/choice/new',
    name: 'ChoiceCreate',
    component: ChoiceUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/choice/:choiceId/edit',
    name: 'ChoiceEdit',
    component: ChoiceUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/choice/:choiceId/view',
    name: 'ChoiceView',
    component: ChoiceDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/results',
    name: 'Results',
    component: Results,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/results/new',
    name: 'ResultsCreate',
    component: ResultsUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/results/:resultsId/edit',
    name: 'ResultsEdit',
    component: ResultsUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/results/:resultsId/view',
    name: 'ResultsView',
    component: ResultsDetails,
    meta: { authorities: [Authority.USER] },
  },

  // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
];
