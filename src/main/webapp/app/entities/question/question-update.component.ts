import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import ChoiceService from '../choice/choice.service';
import { IChoice } from '@/shared/model/choice.model';

import QuizService from '../quiz/quiz.service';
import { IQuiz } from '@/shared/model/quiz.model';

import AlertService from '@/shared/alert/alert.service';
import { IQuestion, Question } from '@/shared/model/question.model';
import QuestionService from './question.service';

const validations: any = {
  question: {
    language: {
      required,
    },
    name: {
      required,
    },
    answersType: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class QuestionUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('questionService') private questionService: () => QuestionService;
  public question: IQuestion = new Question();

  @Inject('choiceService') private choiceService: () => ChoiceService;

  public choices: IChoice[] = [];

  @Inject('quizService') private quizService: () => QuizService;

  public quizzes: IQuiz[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.questionId) {
        vm.retrieveQuestion(to.params.questionId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    this.question.choices = [];
  }

  public save(): void {
    this.isSaving = true;
    if (this.question.id) {
      this.questionService()
        .update(this.question)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('quizApp.question.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.questionService()
        .create(this.question)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('quizApp.question.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveQuestion(questionId): void {
    this.questionService()
      .find(questionId)
      .then(res => {
        this.question = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.choiceService()
      .retrieve()
      .then(res => {
        this.choices = res.data;
      });
    this.quizService()
      .retrieve()
      .then(res => {
        this.quizzes = res.data;
      });
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
