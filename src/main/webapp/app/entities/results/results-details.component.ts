import { Component, Vue, Inject } from 'vue-property-decorator';

import { IResults } from '@/shared/model/results.model';
import ResultsService from './results.service';

@Component
export default class ResultsDetails extends Vue {
  @Inject('resultsService') private resultsService: () => ResultsService;
  public results: IResults = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.resultsId) {
        vm.retrieveResults(to.params.resultsId);
      }
    });
  }

  public retrieveResults(resultsId) {
    this.resultsService()
      .find(resultsId)
      .then(res => {
        this.results = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
