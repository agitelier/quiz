import axios from 'axios';

import buildPaginationQueryOpts from '@/shared/sort/sorts';

import { IResults } from '@/shared/model/results.model';

const baseApiUrl = 'api/results';

export default class ResultsService {
  public find(id: string): Promise<IResults> {
    return new Promise<IResults>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public findAll(): Promise<IResults> {
    return new Promise<IResults>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/list`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieveAllResultsForQuizId(id: string): Promise<IResults> {
    return new Promise<IResults>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/findAllForQuizId/${id}?size=10000`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieveAllResultsForQuizIdAndSubGroupId(id: string, idSubGroup: string): Promise<IResults> {
    return new Promise<IResults>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/findAllForQuizIdAndSubGroupId/${id}/${idSubGroup}?size=10000`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieve(paginationQuery?: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl + `?${buildPaginationQueryOpts(paginationQuery)}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public delete(id: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public create(entity: IResults): Promise<IResults> {
    return new Promise<IResults>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public update(entity: IResults): Promise<IResults> {
    return new Promise<IResults>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public createAnswerFromQuiz(entity: HTMLFormControlsCollection): void {
    const keysArray = new Set();
    for (let i = 0; i <= entity.length; i++) {
      if (entity[i]?.name) {
        if (entity[i]?.name?.indexOf('quizfield-') !== -1) {
          keysArray.add(entity[i].name);
        }
      }
    }

    keysArray.forEach(function (key) {
      const json = {
        email: entity.quizuseremail.value,
        idQuiz: entity.quizid.value,
        idSubGroup: entity?.idSubGroup?.value,
        idQuestion: key.replace('quizfield-', ''),
        answer: entity[key].value,
      };
      if (!entity[key].value && entity[key].length > 0) {
        entity[key].forEach(function (checkbox) {
          if (checkbox.checked) {
            json.answer += checkbox.value + ', ';
          }
        });
      }
      axios({
        method: 'post',
        url: `${baseApiUrl}`,
        data: json,
      });
    });
  }
}
