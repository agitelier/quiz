import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';
import { IResults, Results } from '@/shared/model/results.model';
import ResultsService from './results.service';

const validations: any = {
  results: {
    email: {
      required,
    },
    idQuiz: {
      required,
    },
    idQuestion: {
      required,
    },
    answer: {
      required,
    },
    idSubGroup: {},
  },
};

@Component({
  validations,
})
export default class ResultsUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('resultsService') private resultsService: () => ResultsService;
  public results: IResults = new Results();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.resultsId) {
        vm.retrieveResults(to.params.resultsId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.results.id) {
      this.resultsService()
        .update(this.results)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('quizApp.results.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.resultsService()
        .create(this.results)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('quizApp.results.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public saveNHome(): void {
    this.isSaving = true;
    if (this.results.id) {
      this.resultsService()
        .update(this.results)
        .then(param => {
          this.isSaving = false;
          this.$router.push('/');
          const message = this.$t('quizApp.results.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.resultsService()
        .create(this.results)
        .then(param => {
          this.isSaving = false;
          this.$router.push('/');
          const message = this.$t('quizApp.results.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }
  public retrieveResults(resultsId): void {
    this.resultsService()
      .find(resultsId)
      .then(res => {
        this.results = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
