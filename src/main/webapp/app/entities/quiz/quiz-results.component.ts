import { Component, Vue, Inject } from 'vue-property-decorator';

import { IQuiz } from '@/shared/model/quiz.model';
import { IResults } from '@/shared/model/results.model';
import QuizService from './quiz.service';
import ResultsService from '@/entities/results/results.service';
import AlertService from '@/shared/alert/alert.service';
import AccountService from '@/account/account.service';

@Component
export default class QuizResults extends Vue {
  @Inject('quizService') private quizService: () => QuizService;
  @Inject('resultsService') private resultsService: () => ResultsService;
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('accountService') private accountService: () => AccountService;
  private hasAnyAuthorityValue = false;

  public quiz: IQuiz = {};
  public results: IResults[] = [];

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.quizId) {
        vm.retrieveQuiz(to.params.quizId);
        vm.retrieveAllResultsQuiz(to.params.quizId);
      }
    });
  }

  public retrieveQuiz(quizId) {
    this.quizService()
      .find(quizId)
      .then(res => {
        this.quiz = res;
      });
  }

  public retrieveAllResultsQuiz(quizId): void {
    this.resultsService()
      .retrieveAllResultsForQuizId(quizId)
      .then(res => {
        this.results = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }

  public get getUserEmail(): string {
    return this.$store.getters.account ? this.$store.getters.account.email : '';
  }

  public get getSubGroup(): string {
    return this.$route.params.subgroupId;
  }

  public hasAnyAuthority(authorities: any): boolean {
    this.accountService()
      .hasAnyAuthorityAndCheckAuth(authorities)
      .then(value => {
        this.hasAnyAuthorityValue = value;
      });
    return this.hasAnyAuthorityValue;
  }
}
