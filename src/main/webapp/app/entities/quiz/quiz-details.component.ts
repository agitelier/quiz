import { Component, Vue, Inject } from 'vue-property-decorator';

import { IQuiz } from '@/shared/model/quiz.model';
import QuizService from './quiz.service';
import ResultsService from '@/entities/results/results.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class QuizDetails extends Vue {
  @Inject('quizService') private quizService: () => QuizService;
  @Inject('resultsService') private resultsService: () => ResultsService;
  @Inject('alertService') private alertService: () => AlertService;
  public quiz: IQuiz = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.quizId) {
        vm.retrieveQuiz(to.params.quizId);
      }
    });
  }

  public retrieveQuiz(quizId) {
    this.quizService()
      .find(quizId)
      .then(res => {
        this.quiz = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }

  public submitQuiz(submitEvent): void {
    this.resultsService().createAnswerFromQuiz(submitEvent.target.elements);
    this.$router.push('/quiz');
    const message = this.$t('quizApp.quiz.saved');
    this.alertService().showAlert(message, 'success');
  }

  public get getUserEmail(): string {
    return this.$store.getters.account ? this.$store.getters.account.email : '';
  }

  public get getSubGroup(): string {
    return this.$route.params.subgroupId;
  }
}
