import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import QuestionService from '../question/question.service';
import { IQuestion } from '@/shared/model/question.model';

import AlertService from '@/shared/alert/alert.service';
import { IQuiz, Quiz } from '@/shared/model/quiz.model';
import QuizService from './quiz.service';

const validations: any = {
  quiz: {
    title: {
      required,
    },
    language: {
      required,
    },
    mode: {
      required,
    },
    description: {},
  },
};

@Component({
  validations,
})
export default class QuizUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('quizService') private quizService: () => QuizService;
  public quiz: IQuiz = new Quiz();

  @Inject('questionService') private questionService: () => QuestionService;

  public questions: IQuestion[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.quizId) {
        vm.retrieveQuiz(to.params.quizId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    this.quiz.questions = [];
  }

  public save(): void {
    this.isSaving = true;
    if (this.quiz.id) {
      this.quizService()
        .update(this.quiz)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('quizApp.quiz.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.quizService()
        .create(this.quiz)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('quizApp.quiz.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveQuiz(quizId): void {
    this.quizService()
      .find(quizId)
      .then(res => {
        this.quiz = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.questionService()
      .retrieve()
      .then(res => {
        this.questions = res.data;
      });
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
