import { Component, Vue, Inject } from 'vue-property-decorator';

import { IQuiz } from '@/shared/model/quiz.model';
import { IResults } from '@/shared/model/results.model';
import QuizService from './quiz.service';
import ResultsService from '@/entities/results/results.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class QuizCompare extends Vue {
  @Inject('quizService') private quizService: () => QuizService;
  @Inject('resultsService') private resultsService: () => ResultsService;
  @Inject('alertService') private alertService: () => AlertService;
  public results: IResults = {};
  public quiz: IQuiz = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.quizId) {
        vm.retrieveQuiz(to.params.quizId);
        vm.getResults(to.params.quizId);
      }
    });
  }

  public getNumberOfResults(results, question, answer) {
    let counter = 0;
    let subGroup = this.getSubGroup;
    if (subGroup === undefined) {
      for (const answerNode in results) {
        if (results[answerNode].idQuestion === question && results[answerNode].answer.includes(answer)) {
          counter = counter + 1;
        }
      }
    } else {
      for (const answerNode in results) {
        if (
          results[answerNode].idSubGroup === subGroup &&
          results[answerNode].idQuestion === question &&
          results[answerNode].answer.includes(answer)
        ) {
          counter = counter + 1;
        }
      }
    }
    return counter;
  }

  public getTotalNumber(results, questionID) {
    let total = 0;
    let subGroup = this.getSubGroup;
    if (subGroup === undefined) {
      for (const answerNode in results) {
        if (results[answerNode].idQuestion === questionID) {
          total = total + 1;
        }
      }
    } else {
      for (const answerNode in results) {
        if (results[answerNode].idSubGroup === subGroup && results[answerNode].idQuestion === questionID) {
          total = total + 1;
        }
      }
    }
    return total;
  }

  public getResults(quizId) {
    this.resultsService()
      .retrieveAllResultsForQuizId(quizId)
      .then(res => {
        this.results = res;
      });
  }

  public retrieveQuiz(quizId) {
    this.quizService()
      .find(quizId)
      .then(res => {
        this.quiz = res;
      });
  }

  public previousState() {
    this.$router.push('/quiz');
  }

  public get username(): string {
    return this.$store.getters.account ? this.$store.getters.account.login : '';
  }

  public get getSubGroup(): string {
    return this.$route.params.subgroupId;
  }
}
