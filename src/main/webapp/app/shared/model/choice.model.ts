import { IQuestion } from '@/shared/model/question.model';

export const enum Language {
  FRENCH = 'FRENCH',
  ENGLISH = 'ENGLISH',
}

export interface IChoice {
  id?: string;
  language?: Language;
  displayName?: string;
  value?: number;
  isGood?: boolean;
  questions?: IQuestion[];
}

export class Choice implements IChoice {
  constructor(
    public id?: string,
    public language?: Language,
    public displayName?: string,
    public value?: number,
    public isGood?: boolean,
    public questions?: IQuestion[]
  ) {
    this.isGood = this.isGood || false;
  }
}
