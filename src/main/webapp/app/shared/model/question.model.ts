import { IChoice } from '@/shared/model/choice.model';
import { IQuiz } from '@/shared/model/quiz.model';

export const enum Language {
  FRENCH = 'FRENCH',
  ENGLISH = 'ENGLISH',
}

export const enum AnswersType {
  OPEN = 'OPEN',
  ONE = 'ONE',
  MULTI = 'MULTI',
}

export interface IQuestion {
  id?: string;
  language?: Language;
  name?: string;
  answersType?: AnswersType;
  choices?: IChoice[];
  quizzes?: IQuiz[];
}

export class Question implements IQuestion {
  constructor(
    public id?: string,
    public language?: Language,
    public name?: string,
    public answersType?: AnswersType,
    public choices?: IChoice[],
    public quizzes?: IQuiz[]
  ) {}
}
