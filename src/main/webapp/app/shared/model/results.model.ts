export interface IResults {
  id?: string;
  email?: string;
  idQuiz?: string;
  idQuestion?: string;
  answer?: string;
  idSubGroup?: string;
}

export class Results implements IResults {
  constructor(
    public id?: string,
    public email?: string,
    public idQuiz?: string,
    public idQuestion?: string,
    public answer?: string,
    public idSubGroup?: string
  ) {}
}
