import { IQuestion } from '@/shared/model/question.model';
import { IResults } from '@/shared/model/results.model';

export const enum Language {
  FRENCH = 'FRENCH',
  ENGLISH = 'ENGLISH',
}

export const enum Mode {
  SOLO = 'SOLO',
  GROUP = 'GROUP',
}

export interface IQuiz {
  id?: string;
  title?: string;
  language?: Language;
  mode?: Mode;
  description?: string;
  questions?: IQuestion[];
}

export class Quiz implements IQuiz {
  constructor(
    public id?: string,
    public title?: string,
    public language?: Language,
    public mode?: Mode,
    public description?: string,
    public questions?: IQuestion[]
  ) {}
}
